import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';

const Questions = (props) => {
    let questionsList = props.questions.map(question => {
        return (
            <Text key={question.id}>{question.questionText}</Text>
        );
    })
    return (
        <Text> {questionsList}</Text>

    );
}

function mapStateToProps(state) {
    return {
        questions: state.questions
    };
}

export default connect(mapStateToProps)(Questions);