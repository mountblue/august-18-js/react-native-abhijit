import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Header from './components/Header';
import Questions from './containers/Questions';
import { Provider } from 'react-redux';
import store from './store/store';

export default class App extends React.Component {
  render() {
    return (
      <View>
        <Header />
        <Provider store={store}><Questions /></Provider>
      </View>
    );
  }
}
