import { createStore } from 'redux';
import allReducers from '../reducers/allReducers';

const store = createStore(allReducers);

store.subscribe(() => {
    console.log(store.getState(), 'inside store');
})
store.dispatch({
    type: 'simple'
});

export default store;