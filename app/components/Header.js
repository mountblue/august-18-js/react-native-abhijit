import React from 'react';
import { Stylesheet, Text, View } from 'react-native';

const Header = () => {
    return (
        <View style={{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'flex-end',
            backgroundColor: 'rgb(180, 6, 6)',
            height: 75
        }}>
            <Text style={{
                color: 'white',
                fontSize: 30
            }}>Quora</Text>
        </View>
    );
}

export default Header;