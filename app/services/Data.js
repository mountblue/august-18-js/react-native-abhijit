import React, { Component } from 'react';
import data from './Data.json';



export default class Data extends Component {
    static getQuestions() {
        return [...data];
    }

    static getNewQuestion() {
        this.id = 0;
        this.questionText = '';
        this.answers = [];
        this.answerTextAreaOpen = false;
        this.displayAllAnswersStatus = false;
    }

    static getNewAnswer() {
        this.id = 0;
        this.upvotes = 0;
        this.downvotes = 0;
        this.answerText = '';
        this.author = "Drew Houston";
    }
}