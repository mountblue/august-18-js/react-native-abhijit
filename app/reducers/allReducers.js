import { combineReducers } from 'redux';
import questions from './reducerQuestion';

const allReducers = combineReducers({
    questions: questions
});

export default allReducers;
